package br.com.senac.sd.trab1.cantinaclient.entities;

class MessageContainer<T> {
    MessageType type
    T data

    MessageContainer(MessageType type, T data) {
        this.type = type
        this.data = data
    }
}
