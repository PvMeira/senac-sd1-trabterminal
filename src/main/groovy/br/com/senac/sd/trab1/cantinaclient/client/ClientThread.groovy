package br.com.senac.sd.trab1.cantinaclient.client

import br.com.senac.sd.trab1.cantinaclient.entities.Client
import br.com.senac.sd.trab1.cantinaclient.entities.CurrentState
import br.com.senac.sd.trab1.cantinaclient.entities.MessageType
import br.com.senac.sd.trab1.cantinaclient.entities.OrderListItem
import groovy.json.JsonSlurper

import java.util.function.Function

class ClientThread extends Thread {
    private Socket socket
    private BufferedReader bufferedReader
    private PrintWriter printWriter

    private SocketListener socketListener

    private boolean stateChange
    private Client loggedClient
    private String serverMessage
    private OrderListItem orderListItem
    private CurrentState currentState

    private Function<Client, Void> onLoginResponse
    private Function<Void, Void> onLogoutResponse
    private Function<OrderListItem, Void> onWorkerOrderResponse
    private Function<OrderListItem, Void> onWorkerOrderResponseNull

    ClientThread(Socket socket) {
        this.socket = socket
        this.bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()))
        this.printWriter = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()))

        socketListener = new SocketListener(this, bufferedReader)
        socketListener.start()

        this.stateChange = true
        this.currentState = CurrentState.STATE_GUEST
    }

    void run() {
        try {
            while (!this.isInterrupted()) {
                if (stateChange) {
                    this.stateChange = false
                }
            }
        } catch (Exception ignore) {

        }
    }

    synchronized void setWorkerOrderResponseCallback(Function<OrderListItem, Void> f) {
        this.onWorkerOrderResponse = f
    }
    synchronized void setWorkerOrderResponseNullCallback(Function<OrderListItem, Void> f) {
        this.onWorkerOrderResponseNull = f
    }

    synchronized void setLoginResponseCallback(Function<Client, Void> f) {
        this.onLoginResponse = f
    }

    synchronized void setLogoutResponseCallback(Function<Void, Void> f) {
        this.onLogoutResponse = f
    }

    synchronized void processMessage(String message) {
        Map messageMap = new JsonSlurper().parseText(message) as Map
        stateChange = true
        serverMessage = null
        switch (messageMap['type'] as MessageType) {
            case MessageType.MESSAGE_KEEP_ALIVE_RESPONSE:
                // Keep Alive Response
                break

            case MessageType.MESSAGE_LOGIN_RESPONSE:
                loggedClient = messageMap['data'] as Client
                if (this.onLoginResponse != null)
                    this.onLoginResponse.apply(loggedClient)
                break

            case MessageType.MESSAGE_LOGOUT_RESPONSE:
                loggedClient = null
                if (this.onLogoutResponse != null)
                    this.onLogoutResponse.apply(null)
                break

            case MessageType.MESSAGE_ITEMS_RESPONSE:
                // Ignore
                break

            case MessageType.MESSAGE_CHECKOUT_RESPONSE:
                // Ignore
                break

            case MessageType.MESSAGE_WORKER_GET_ORDER_RESPONSE:
                if (messageMap['data'] == null || messageMap['data']['order'] == null) {
                    // No more orders, do something
                    orderListItem = null
                    this.onWorkerOrderResponseNull.apply(orderListItem)
                } else {
                    // remove unused key causing json issue
                    (messageMap['data']['order'] as Map).remove('total')

                    orderListItem = messageMap['data'] as OrderListItem
                    if (orderListItem != null && this.onWorkerOrderResponse != null)
                        this.onWorkerOrderResponse.apply(orderListItem)
                }
                break

            case MessageType.MESSAGE_WORKER_FINISH_ORDER_RESPONSE:
//                this.onWorkerOrderResponseNull.apply(null)
                //TODO: Request new order?
                break

            default:
                System.err.println("[System] Wrong message type: " + messageMap['type'])
                break
        }
    }

    synchronized void sendMessage(String message) {
        printWriter.println(message)
        printWriter.flush()
    }
}
