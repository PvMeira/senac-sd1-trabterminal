package br.com.senac.sd.trab1.cantinaclient.entities

class OrderItem {
    String id
    int quantity

    OrderItem(String id, int quantity) {
        this.id = id
        this.quantity = quantity
    }
}
