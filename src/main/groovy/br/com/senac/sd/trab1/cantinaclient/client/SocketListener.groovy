package br.com.senac.sd.trab1.cantinaclient.client

import org.apache.commons.lang3.StringUtils

class SocketListener extends Thread {
    private ClientThread clientThread
    private BufferedReader mReader

    SocketListener(ClientThread clientThread, BufferedReader aReader) {
        this.clientThread = clientThread
        this.mReader = aReader
    }

    void run() {
        try {
            while (!this.isInterrupted()) {
                String data = mReader.readLine()
                if (StringUtils.isBlank(data))
                    continue

                clientThread.processMessage(data)
            }
        } catch (IOException ignore) {
            System.err.println("Lost connection to server.")
            System.exit(-1)
        }
    }
}
