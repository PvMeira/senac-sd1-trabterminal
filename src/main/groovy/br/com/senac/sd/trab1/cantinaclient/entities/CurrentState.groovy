package br.com.senac.sd.trab1.cantinaclient.entities

enum CurrentState {
    STATE_GUEST,
    STATE_USERNAME_INPUT,
    STATE_PASSWORD_INPUT,
    STATE_USER_MENU,
    STATE_ITEM_MENU
}
