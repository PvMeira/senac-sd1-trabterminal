package br.com.senac.sd.trab1.cantinaclient.tools

import javax.swing.*
import java.awt.*

/**
 * Created by pvmeira on 24/06/17.
 */
class MessageUtil {
    static void errorMessage(Component frame, String titulo, String content) {
        JOptionPane.showMessageDialog(frame, content, titulo, JOptionPane.ERROR_MESSAGE)
    }

    static void sucessMessage(Component frame, String titulo, String content) {
        JOptionPane.showMessageDialog(frame, content, titulo, JOptionPane.INFORMATION_MESSAGE)
    }

    static void warningMessage(Component frame, String titulo, String content) {
        JOptionPane.showMessageDialog(frame, content, titulo, JOptionPane.WARNING_MESSAGE)
    }
}
