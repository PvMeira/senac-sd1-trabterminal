package br.com.senac.sd.trab1.cantinaclient.ui

import br.com.senac.sd.trab1.cantinaclient.entities.Client
import br.com.senac.sd.trab1.cantinaclient.entities.Order
import br.com.senac.sd.trab1.cantinaclient.entities.OrderDTO
import br.com.senac.sd.trab1.cantinaclient.tools.MessageUtil

import javax.swing.*
import javax.swing.table.DefaultTableModel
import java.awt.*
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import java.util.List
import java.util.function.Function

class TerminalInterface extends JFrame implements ActionListener {
    private JPanel jPanel
    private JTable jTable
    private JScrollPane jScrollPane
    private Order menu
    private DefaultTableModel m
    private Client client
    private Client orderClient
    private OrderDTO orderDTO
    private LoginInterface loginInterface
    private JButton logout

    private Function<List<String>, Void> onAceptOrderClick

    TerminalInterface(Client client, Order order, Function<List<String>, Void> onAceptOrderClick, Client orderClient,LoginInterface l) {
        super("Pedidos")
        this.client = client
        this.menu = order
        this.orderClient = orderClient
        this.onAceptOrderClick = onAceptOrderClick
        this.loginInterface = l
        createJTable()
        createWindow()
    }

    void display() {
        this.setVisible(true)
    }

    void createWindow() {
        jScrollPane = new JScrollPane(jTable)

        jPanel = new JPanel()
        jPanel.setLayout(new BorderLayout())
        jPanel.add(BorderLayout.CENTER, jScrollPane)

        getContentPane().add(jPanel)

        setDefaultCloseOperation(EXIT_ON_CLOSE)
        setSize(500, 320)
        logout = new JButton("Sair")
        logout.setActionCommand("sair")
        logout.setSize(133,200)
        getContentPane().add(logout, BorderLayout.WEST)
        logout.addActionListener(this)

    }


    private void createJTable() {
        m = new DefaultTableModel() {
            @Override
            Class<?> getColumnClass(int columnIndex) {
                return columnIndex == 0 ? Boolean.class : String.class
            }

            @Override
            boolean isCellEditable(int row, int column) {
                return column == 0
            }
        }
        //TODO edit for Order
        jTable = new JTable()
        jTable.setModel(m)

        m.addColumn("")
        m.addColumn("Id")
        m.addColumn("Quantidade")

        jTable.getColumnModel().getColumn(0).setPreferredWidth(5)
        jTable.getColumnModel().getColumn(1).setPreferredWidth(10)
        jTable.getColumnModel().getColumn(2).setPreferredWidth(120)


        this.fillOrder(m)

        JButton selectRows = new JButton("Aprovar pedido")
        selectRows.addActionListener(this)
        selectRows.setSize(100, 50)
        getContentPane().add(BorderLayout.SOUTH, selectRows)
    }

    void fillOrder(DefaultTableModel tableModel) {
        tableModel.setNumRows(0)
        if (this.menu != null) {
            for (item in this.menu.items) {
                Vector vector = new Vector()
                vector.add(false)
                vector.add(item.id)
                vector.add(item.quantity)
                tableModel.addRow(vector)
            }
        } else {
        }

    }

    @Override
    void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equalsIgnoreCase(logout.getActionCommand())){
            this.dispose()
            loginInterface.display()
        }else {
            List<String> ids = new ArrayList<>()

            for (int i = 0; i < jTable.getRowCount(); i++) {
                Boolean isBuyingRow = Boolean.valueOf(jTable.getValueAt(i, 0).toString())
                String itemRow = jTable.getValueAt(i, 1).toString()
                if (isBuyingRow)
                    ids.add(itemRow)
            }

            if (ids.isEmpty()) {
                MessageUtil.errorMessage(null, "Aprovar  - Erro", "Nenhum Item Selecionado")
                return
            }
            //TODO edit for Order
            if (this.onAceptOrderClick != null)
                this.onAceptOrderClick.apply(ids)
        }
    }
}
