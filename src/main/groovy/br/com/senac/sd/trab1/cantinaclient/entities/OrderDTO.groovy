package br.com.senac.sd.trab1.cantinaclient.entities

/**
 * Created by pvmeira on 06/07/17.
 */
class OrderDTO {

    Order order

    Client clientOrder

    Order getOrder() {
        return order
    }

    void setOrder(Order order) {
        this.order = order
    }

    Client getClientOrder() {
        return clientOrder
    }

    void setClientOrder(Client clientOrder) {
        this.clientOrder = clientOrder
    }
}
