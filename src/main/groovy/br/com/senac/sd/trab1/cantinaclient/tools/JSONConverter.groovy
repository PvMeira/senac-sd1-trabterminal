package br.com.senac.sd.trab1.cantinaclient.tools

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule
import org.apache.commons.lang3.StringUtils

import java.text.SimpleDateFormat

class JSONConverter {

    static String fromEntity(Object obj) {
        ObjectMapper mapper = getObjectMapper()
        try {
            return mapper.writeValueAsString(obj)
        } catch (JsonProcessingException ex) {
            System.err.println("[System] Error while converting Entity to JSON: " + ex.getMessage())
            ex.printStackTrace()
            return null
        }
    }

    static Object toEntity(String source, Class clazz) {
        if (StringUtils.isBlank(source))
            return null

        ObjectMapper mapper = getObjectMapper()
        try {
            return mapper.readValue(source, clazz.getClass())
        } catch (IOException ex) {
            System.err.println("[System] Error while converting JSON to Entity: " + ex.getMessage())
            ex.printStackTrace()
            return null
        }
    }

    private static ObjectMapper getObjectMapper() {
        ObjectMapper mapper = new ObjectMapper()
        mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd"))

        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL)
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
        mapper.configure(SerializationFeature.WRITE_DATE_TIMESTAMPS_AS_NANOSECONDS, false)
        mapper.configure(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE, false)
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)

        mapper.registerModule(new ParameterNamesModule())
        mapper.registerModule(new Jdk8Module())
        mapper.registerModule(new JavaTimeModule())

        return mapper
    }
}
