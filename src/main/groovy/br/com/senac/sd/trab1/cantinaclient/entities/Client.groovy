package br.com.senac.sd.trab1.cantinaclient.entities

class Client {
    String username
    ClientRole role
    double balance

    boolean equals(o) {
        if (this.is(o)) return true
        if (getClass() != o.class) return false

        Client client = (Client) o

        if (balance != client.balance) return false
        if (role != client.role) return false
        if (username != client.username) return false

        return true
    }

    int hashCode() {
        int result
        result = (username != null ? username.hashCode() : 0)
        result = 31 * result + (role != null ? role.hashCode() : 0)
        result = 31 * result + (int) (balance ^ (balance >>> 32))
        return result
    }
}