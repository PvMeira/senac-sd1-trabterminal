package br.com.senac.sd.trab1.cantinaclient

import br.com.senac.sd.trab1.cantinaclient.controller.UIController

class Main {
    static void main(String[] args) {
        UIController uiController = new UIController()
        uiController.show()
    }
}
