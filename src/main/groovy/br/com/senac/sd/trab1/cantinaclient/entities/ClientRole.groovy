package br.com.senac.sd.trab1.cantinaclient.entities

enum ClientRole {
    CLIENT,
    WORKER
}