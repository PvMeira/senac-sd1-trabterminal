package br.com.senac.sd.trab1.cantinaclient.controller

import br.com.senac.sd.trab1.cantinaclient.client.ClientThread
import br.com.senac.sd.trab1.cantinaclient.entities.Client
import br.com.senac.sd.trab1.cantinaclient.entities.MessageType
import br.com.senac.sd.trab1.cantinaclient.entities.OrderListItem
import br.com.senac.sd.trab1.cantinaclient.tools.JSONConverter
import br.com.senac.sd.trab1.cantinaclient.tools.MessageUtil
import br.com.senac.sd.trab1.cantinaclient.ui.ConfigInterface
import br.com.senac.sd.trab1.cantinaclient.ui.LoginInterface
import br.com.senac.sd.trab1.cantinaclient.ui.TerminalInterface

import javax.swing.*

@SuppressWarnings("GrMethodMayBeStatic")
class UIController {

    private JFrame mainJFrame
    private ConfigInterface configInterface
    private LoginInterface loginInterface
    private TerminalInterface terminalInterface

    private Socket socket
    private ClientThread clientThread
    private Client loggedClient

    UIController() {
        mainJFrame = new JFrame()
        configInterface = null
    }

    void show() {
        if (configInterface == null)
            configInterface = new ConfigInterface(mainJFrame, {
                host, ip ->
                    this.onConnect(host, ip)
            })

        configInterface.display()
    }

    private showLogin() {
        if (loginInterface == null)
            loginInterface = new LoginInterface(mainJFrame, {
                username, password ->
                    this.onLogin(username, password)
            })

        loginInterface.display()
        mainJFrame.setVisible(false)
    }

    private showTerminal(OrderListItem orderList) {
        // TODO: Aqui chega a order do cliente...
        if (terminalInterface == null) {
            if(orderList == null){
                MessageUtil.warningMessage(null,"Sem Pedidos","Sem pedidos")
                terminalInterface = new TerminalInterface(loggedClient, null, {
                    ids -> this.acceptWorkerOrder()
                }, null,loginInterface)
                terminalInterface.display()

            }else {
                terminalInterface = new TerminalInterface(loggedClient, orderList.order, {
                    ids -> this.acceptWorkerOrder()
                }, orderList.client,loginInterface)
                terminalInterface.display()
            }
        } else {
            if(orderList == null){
                terminalInterface.dispose()
                MessageUtil.warningMessage(null,"Sem Pedidos","Sem pedidos")
                terminalInterface = new TerminalInterface(loggedClient, null, {
                    ids -> this.acceptWorkerOrder()
                }, null,loginInterface)
                terminalInterface.display()
            }else {
                terminalInterface.dispose()
                terminalInterface = new TerminalInterface(loggedClient, orderList.order, {
                    ids -> this.acceptWorkerOrder()
                }, orderList.client,loginInterface)
                terminalInterface.display()
            }
        }

        mainJFrame.setVisible(false)
    }

    private onConnect(String host, Integer port) {
        try {
            this.socket = new Socket(host, port)
            this.clientThread = new ClientThread(socket)
            this.clientThread.setLoginResponseCallback({ c -> this.onLoginResponse(c) })
            this.clientThread.setLogoutResponseCallback({ this.onLoginResponse(null) })
            this.clientThread.setWorkerOrderResponseCallback({ w -> this.onWorkerOrderReceived(w) })
            this.clientThread.setWorkerOrderResponseNullCallback({ w -> this.onWorkerOrderReceived(w) })
            this.showLogin()
        } catch (IOException ex) {
            MessageUtil.errorMessage(this.mainJFrame, "Erro ao conectar-se", ex.getMessage())
        } catch (Exception ex2) {
            MessageUtil.errorMessage(this.mainJFrame, "Error", ex2.getMessage())
        }
    }

    private void onWorkerOrderReceived(OrderListItem orderListItem) {
        // TODO: Aqui tu tem o OrderListItem que contem a order e o cliente que quer ela... da um jeito de mostrar as infos.
        if (orderListItem == null)
            this.mainJFrame.dispose()
        this.showTerminal(orderListItem)
    }

    private void onLoginResponse(Client client) {
        loggedClient = client
        if (loggedClient != null)
            this.requestOrder()
        else
            this.mainJFrame.dispose()
    }

    private void onLogin(String username, String password) {
        Map map = [
                'type': MessageType.MESSAGE_LOGIN_REQUEST,
                'data': [
                        'username': username,
                        'password': password
                ]
        ]

        String message = JSONConverter.fromEntity(map)
        clientThread.sendMessage(message)
    }

    private void requestOrder() {
        Map map = [
                'type': MessageType.MESSAGE_WORKER_GET_ORDER_REQUEST,
        ]

        String message = JSONConverter.fromEntity(map)
        clientThread.sendMessage(message)
    }

    private void acceptWorkerOrder() {
        Map map = [
                'type': MessageType.MESSAGE_WORKER_FINISH_ORDER_REQUEST
        ]

        String message = JSONConverter.fromEntity(map)
        clientThread.sendMessage(message)
        this.requestOrder()
    }
}
