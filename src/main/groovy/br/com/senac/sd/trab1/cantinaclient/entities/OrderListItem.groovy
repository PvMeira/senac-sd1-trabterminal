package br.com.senac.sd.trab1.cantinaclient.entities

class OrderListItem {
    String id
    Order order
    Client client

    boolean equals(o) {
        if (this.is(o)) return true
        if (getClass() != o.class) return false

        OrderListItem that = (OrderListItem) o

        if (client != that.client) return false
        if (id != that.id) return false
        if (order != that.order) return false

        return true
    }

    int hashCode() {
        int result
        result = (id != null ? id.hashCode() : 0)
        result = 31 * result + (order != null ? order.hashCode() : 0)
        result = 31 * result + (client != null ? client.hashCode() : 0)
        return result
    }
}
