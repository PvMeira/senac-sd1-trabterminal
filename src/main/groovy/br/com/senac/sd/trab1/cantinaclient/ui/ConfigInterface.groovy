package br.com.senac.sd.trab1.cantinaclient.ui

import br.com.senac.sd.trab1.cantinaclient.tools.MessageUtil
import org.apache.commons.lang3.StringUtils

import javax.swing.*
import java.awt.*
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import java.util.function.BiFunction

class ConfigInterface extends JFrame implements ActionListener {
    JTextField hostField
    JLabel hostLabel
    JLabel portLabel
    JTextField portField
    JFrame frame
    JButton btnLogin

    BiFunction<String, Integer, Void> onConnectClick

    ConfigInterface(JFrame jFrame, BiFunction<String, Integer, Void> onConnectClick) {
        this.frame = jFrame
        this.onConnectClick = onConnectClick

        this.hostLabel = new JLabel("IP : ")
        this.frame.add(hostLabel)

        this.hostField = new JTextField("localhost", 10)
        this.frame.add(hostField)

        this.portLabel = new JLabel("Port : ")
        this.frame.add(portLabel)

        this.portField = new JTextField("6000", 10)
        this.frame.add(portField)

        this.btnLogin = new JButton("Go to Login")
    }

    void display() {
        this.frame.setDefaultCloseOperation(EXIT_ON_CLOSE)
        this.frame.setSize(500, 300)
        this.frame.setLayout(new FlowLayout())
        this.frame.getContentPane().add(btnLogin)
        this.frame.setVisible(true)
        this.btnLogin.addActionListener(this)
    }

    @Override
    void actionPerformed(ActionEvent e) {
        if (!StringUtils.isBlank(hostField.getText()) && StringUtils.isNumeric(portField.getText())) {
            if (this.onConnectClick != null)
                this.onConnectClick.apply(hostField.getText(), Integer.valueOf(portField.getText()))
            else
                MessageUtil.errorMessage(frame, "Erro", "Isso não deveria acontecer")
        } else {
            MessageUtil.errorMessage(frame, "Erro", "Digita um host e porta correto seu cotoco")
        }
    }
}
