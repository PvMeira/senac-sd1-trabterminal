package br.com.senac.sd.trab1.cantinaclient.client

import jline.console.ConsoleReader
import org.apache.commons.lang3.StringUtils

class ConsoleListener extends Thread {
    private ClientThread clientThread
    private ConsoleReader mReader

    ConsoleListener(ClientThread clientThread, ConsoleReader aReader) {
        this.clientThread = clientThread
        this.mReader = aReader
    }

    void run() {
        try {
            while (!this.isInterrupted()) {
                String data = mReader.readLine("Option: ")
                if (StringUtils.isBlank(data))
                    continue

                clientThread.processConsole(data)
            }
        } catch (IOException ignore) {
            System.exit(-1)
        }
    }
}
