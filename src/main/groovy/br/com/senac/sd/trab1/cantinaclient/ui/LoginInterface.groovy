package br.com.senac.sd.trab1.cantinaclient.ui

import javax.swing.*
import javax.swing.border.LineBorder
import java.awt.*
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import java.util.function.BiFunction

/**
 * Created by pvmeira on 24/06/17.
 */
class LoginInterface extends JDialog implements ActionListener {
    private JTextField tfUsername
    private JPasswordField pfPassword
    private JLabel lbUsername
    private JLabel lbPassword
    private JButton btnLogin
    private JButton btnCancel

    private BiFunction<String, String, Void> onLoginClick

    LoginInterface(Frame parent, BiFunction<String, String, Void> onLoginClick) {
        super(parent, "Login", true)

        this.onLoginClick = onLoginClick

        JPanel panel = new JPanel(new GridBagLayout())
        GridBagConstraints cs = new GridBagConstraints()

        cs.fill = GridBagConstraints.HORIZONTAL

        lbUsername = new JLabel("Username: ")
        cs.gridx = 0
        cs.gridy = 0
        cs.gridwidth = 1
        panel.add(lbUsername, cs)

        tfUsername = new JTextField(20)
        cs.gridx = 1
        cs.gridy = 0
        cs.gridwidth = 2
        panel.add(tfUsername, cs)

        lbPassword = new JLabel("Password: ")
        cs.gridx = 0
        cs.gridy = 1
        cs.gridwidth = 1
        panel.add(lbPassword, cs)

        pfPassword = new JPasswordField(20)
        cs.gridx = 1
        cs.gridy = 1
        cs.gridwidth = 2
        panel.add(pfPassword, cs)
        panel.setBorder(new LineBorder(Color.GRAY))

        btnLogin = new JButton("Login")

        btnLogin.addActionListener(this)
        btnCancel = new JButton("Cancel")
        btnCancel.addActionListener(this)

        JPanel bp = new JPanel()
        bp.add(btnLogin)
        bp.add(btnCancel)

        getContentPane().add(panel, BorderLayout.CENTER)
        getContentPane().add(bp, BorderLayout.PAGE_END)

        pack()
        setResizable(false)
        setLocationRelativeTo(parent)
    }

    String getUsername() {
        return tfUsername.getText().trim()
    }

    String getPassword() {
        return new String(pfPassword.getPassword())
    }

    void display() {
        tfUsername.setText("")
        pfPassword.setText("")
        this.setVisible(true)
    }

    @Override
    void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equalsIgnoreCase(btnCancel.getActionCommand()))
            this.dispose()

        if (this.onLoginClick != null) {
            this.onLoginClick.apply(getUsername(), getPassword())
        }
    }
}
