package br.com.senac.sd.trab1.cantinaclient.entities

class MenuItem {
    String id
    String name
    double price

    MenuItem(String id, String name, double price) {
        this.id = id
        this.name = name
        this.price = price
    }

    @Override
    boolean equals(Object obj) {
        MenuItem item = (MenuItem) obj
        return id.equalsIgnoreCase(item.id)
    }
}
